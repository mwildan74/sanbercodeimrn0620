var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

var time = 3000
var index = 0
function baca(time, books) {
    if (time > 0 && index < books.length)
        readBooks(time, books[index], function (sisaWaktu) {
            time = sisaWaktu;
            index++
            baca(time, books)
        })
}

baca(time, books)