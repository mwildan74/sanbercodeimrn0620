var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]
var time = 3000
var index = 0

function bacaPromise(time, books) {
    if (time > 0 && index < books.length)
        readBooksPromise(time, books[index])
            .then(function (sisaWaktu) {
                time = sisaWaktu
                index++
                bacaPromise(time, books)
            })
            .catch(function (sisaWaktu) {
                time = sisaWaktu
                index++
                bacaPromise(time.books)
            })
}
bacaPromise(time, books)