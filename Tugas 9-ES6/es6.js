// Nomber1
console.log(`=======Nomber1=======`)
const golden = () => {
    console.log(`this is golden!!`)
}
golden()

// Nomber2
console.log(`=======Nomber2=======`)

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`)
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

// Nomber3
console.log(`=======Nomber3=======`)
const newObject = {
    firstName: `Harry`,
    lastName: `Potter Holt`,
    destination: `Hogwarts React Conf`,
    onccupation: `Deve-wizard Avocado`,
    spell: `Vimulus Renderus!!!`
}
const { firstName, lastName, destination, onccupation, spell } = newObject

console.log(firstName, lastName, destination, onccupation)

// Nomber4
console.log(`=======Nomber4=======`)

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]

console.log(combined)

// Nomber5

console.log(`=======Nomber5=======`)

const planet = "earh"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
var after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before)
console.log(after)