// Nomber 1
console.log("=======Nomber1=======");

function teriak() {
  return "Halo Sanbers!";
}
console.log(teriak());

// Nomber 2
console.log("=======Nomber2=======");

function kali(num1, num2) {
  return num1 * num2;
}
var hasilkali = kali(12, 4);
console.log(hasilkali);

// Nomber 3
console.log("=======Nomber3=======");

function introduce(name, age, address, hobby) {
  var kalimat =
    "Nama Saya " +
    name +
    ", " +
    "umur saya " +
    age +
    " tahun," +
    " alamat saya di" +
    address +
    ", " +
    "dan saya punya hobby yaitu " +
    hobby;
  return kalimat;
}
var name = "Agus";
var age = 30;
var address = " Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
