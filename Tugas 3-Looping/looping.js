// Nomber 1
console.log("=======Nomber1=======");
console.log("LOOPING PERTAMA");
var nilai = 2;
while (nilai <= 20) {
  console.log(nilai + " - " + "I love coding");
  nilai += 2;
}
console.log("LOOPING KEDUA");
var nilai2 = 20;
while (nilai2 >= 2) {
  console.log(nilai2 + " - " + "I will become a mobile developer");
  nilai2 -= 2;
}

// Nomber 2
console.log("=======Nomber2=======");
for (var i = 1; i <= 20; i++) {
  if (i % 2 == 1 && i % 3 == 0) {
    console.log(i + " - " + "I Love Coding");
  } else if (i % 2 == 1) {
    console.log(i + " - " + "Santai");
  } else {
    console.log(i + " - " + "Berkualitas");
  }
}

// Nomber 3
console.log("=======Nomber3=======");

var space = "";
for (var baris = 1; baris <= 4; baris++) {
  for (var kolom = 1; kolom <= 8; kolom++) {
    space = space + "#";
  }
  space = space + "\n";
}
console.log(space);

//  Nomber 4
console.log("=======Nomber4=======");

var space2 = "";
for (var j = 1; j <= 7; j++) {
  space2 = space2 + "#";
  console.log(space2);
}

//  Nomber 5
console.log("=======Nomber5=======");
var space3 = "";

for (var baris1 = 1; baris1 <= 8; baris1++) {
  for (var kolom1 = 1; kolom1 <= 4; kolom1++) {
    if (baris1 % 2 == 1) {
      space3 = space3 + " #";
    } else {
      space3 = space3 + "# ";
    }
  }
  space3 = space3 + "\n";
}

console.log(space3);
