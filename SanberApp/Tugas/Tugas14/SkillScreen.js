import React, { Component } from 'react';
import { StyleSheet, View, Image, TouchableOpacity, Text, FlatList } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import LanguageItem from './LanguageItem';
import FrameWork from './FrameWork';
import Technologi from './Technologi';
import data from './skillData.json';


export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <Text style={{ fontWeight: 'bold', fontSize: 20 }}>Will Smith</Text>
                    <View style={styles.leftBar}>
                        <FontAwesome name="user-circle" size={50} color="black" />
                    </View>
                </View>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}> SKILL</Text>
                <View style={styles.itemLg}>
                    <Text style={{ fontSize: 15, color: '#AAA6A6', fontWeight: 'bold', fontFamily: 'Roboto', textAlign: 'center', }}>Language</Text>
                    {/* <LanguageItem language={data.language[0]} /> */}
                    <FlatList
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        data={data.language}
                        renderItem={(language) => <LanguageItem language={language.item} />}
                        keyExtractor={(language) => language.id.toString()}
                    />
                </View>
                <View style={styles.itemFr}>
                    <Text style={{ fontSize: 15, color: '#AAA6A6', fontWeight: 'bold', fontFamily: 'Roboto', textAlign: 'center' }}>Framework</Text>
                    <FlatList
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        data={data.framework}
                        renderItem={(framework) => <FrameWork framework={framework.item} />}
                        keyExtractor={(framework) => framework.id.toString()}
                    />
                </View>
                <View style={styles.itemTc}>
                    <Text style={{ fontSize: 15, color: '#AAA6A6', fontWeight: 'bold', fontFamily: 'Roboto', textAlign: 'center' }}>Technology</Text>
                    <FlatList
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        data={data.technology}
                        renderItem={(technology) => <Technologi technology={technology.item} />}
                        keyExtractor={(technology) => technology.id.toString()}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
    },
    navBar: {
        backgroundColor: 'white',
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    itemLg: {
        flex: 1,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 15,


    },
    itemFr: {
        flex: 1,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 15,
    },
    itemTc: {
        flex: 1,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 15
    },

});
