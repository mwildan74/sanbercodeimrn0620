import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

export const About = () => {
    return (
        <View>
            <TouchableOpacity style={styles.imageBack1}>
                <Image source={require('./assets/left.png')} style={styles.imageBack} />
            </TouchableOpacity>
            <View style={styles.UserPic}>
                <Text style={styles.textJudul}>About Me</Text>
                <FontAwesome name="user-circle" size={124} color="#FFC700" />
            </View>
            <View style={{ paddingBottom: 10, marginLeft: 37 }}>
                <Text style={{ fontFamily: "Roboto", fontWeight: "bold", fontSize: 18, color: "#AAA6A6" }}>Name</Text>
                <Text style={{ fontFamily: "Roboto", fontWeight: "bold", fontSize: 16, paddingLeft: 10, paddingTop: 5 }}>Will Smith</Text>
            </View>
            <View style={{ paddingBottom: 10, marginLeft: 37 }}>
                <Text style={{ fontFamily: "Roboto", fontWeight: "bold", fontSize: 18, color: "#AAA6A6" }}>Bord Date</Text>
                <Text style={{ fontFamily: "Roboto", fontWeight: "bold", fontSize: 16, paddingLeft: 10, paddingTop: 5 }}>12-Agustus-1995</Text>
            </View>

            <Text style={{ fontFamily: "Roboto", fontWeight: "bold", fontSize: 18, color: "#AAA6A6", textAlign: 'center', paddingBottom: 15 }} >My Social Media</Text>
            <View style={styles.icon}>
                <AntDesign name="instagram" size={50} color="black" style={styles.box} />
                <AntDesign name="facebook-square" size={50} color="black" style={styles.box} />
                <AntDesign name="twitter" size={50} color="black" style={styles.box} />
            </View>

            <Text style={{ fontFamily: "Roboto", fontWeight: "bold", fontSize: 18, color: "#AAA6A6", textAlign: 'center', paddingBottom: 15, paddingTop: 15 }}>My Portofolio</Text>
            <View style={styles.icon}>
                <AntDesign name="gitlab" size={50} color="black" style={styles.box} />
                <AntDesign name="linkedin-square" size={50} color="black" style={styles.box} />
                <AntDesign name="github" size={50} color="black" style={styles.box} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    imageBack1: {
        height: 40,
        width: 40,
    },
    imageBack: {
        height: 25,
        width: 25,
        left: 20,
        top: 30
    },
    UserPic: {
        alignItems: 'center'
    },
    textJudul: {
        fontFamily: "Roboto",
        fontWeight: 'bold',
        fontSize: 24,
        textAlign: 'center',
        marginTop: 65,
        paddingBottom: 20
    },
    data: {
        marginLeft: 37,
        marginTop: 20,
    },
    sosmed: {
        paddingTop: 10,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    box: {
        paddingLeft: 20
    },
    icon: {
        flexDirection: 'row',
        justifyContent: 'center',
    }


})