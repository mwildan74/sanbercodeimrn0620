import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { ScreenContainer } from 'react-native-screens';

import { login, Home } from './LoginScreen';
import { Project } from './ProjectScreen';
import { AddScreen } from './AddScreen';
import { About } from './AboutScreen';
import { Skill } from './SkillScreen';

const AuthStack = createStackNavigator();

// export default () => {
//     return (
//         <NavigationContainer>
//             <AuthStack.Navigator>
//                 <AuthStack.Screen name="login" component={login} />
//                 <AuthStack.Screen name="Home" component={Home} />
//             </AuthStack.Navigator>
//         </NavigationContainer>
//     )
// }

const Tabs = createBottomTabNavigator();
const TabScreen = () => {
    return (
        <Tabs.Navigator>
            <Tabs.Screen name="Skill" component={Skill} />
            <Tabs.Screen name="AddScreen" component={AddScreen} />
            <Tabs.Screen name="Project" component={Project} />
        </Tabs.Navigator>
    )
}
const Drawer = createDrawerNavigator();

const DrawerScreen = () => {
    return (
        <Drawer.Navigator>
            <Drawer.Screen name="Home" component={TabScreen} />
            <Drawer.Screen name="About" component={About} />
        </Drawer.Navigator>
    )
}
export default () => {
    return (
        <NavigationContainer>
            <AuthStack.Navigator>
                <AuthStack.Screen name="login" component={login} options={{ headerShown: false }} />
                <AuthStack.Screen name="Drawer" component={DrawerScreen} options={{ headerShown: false }} />
            </AuthStack.Navigator>
        </NavigationContainer>
    )
}

