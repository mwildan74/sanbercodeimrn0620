import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Technologi extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        let technology = this.props.technology
        return (
            <View style={styles.container}>
                <View style={styles.wrapper}>
                    <Icon name={technology.iconName} size={70} />
                    <View style={styles.textBox}>
                        <Text style={{ fontSize: 15 }}>{technology.skillName}</Text>
                        <Text style={{ fontSize: 10, color: '#AAA6A6' }}>{technology.categoryName}</Text>
                        <Text>{technology.percentageProgress}</Text>
                    </View>
                </View>
            </View>

        )

    }
}

const styles = StyleSheet.create({
    container: {
        paddingLeft: 15,
        paddingTop: 0,
        flexDirection: 'row',
    },
    wrapper: {
        height: 100,
        width: 100,
        backgroundColor: '#FFC700',
        alignItems: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,

    },
    textBox: {
        backgroundColor: 'white',
        height: 70,
        width: 100,
        elevation: 10,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        alignItems: 'center',
        paddingTop: 0
    }


});