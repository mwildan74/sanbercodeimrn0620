import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';

export default class App extends Component {
    render() {
        return (
            <View>
                <Image source={require('./assets/login.png')} style={styles.imagelogin} />
                <TouchableOpacity style={styles.imageBack1}>
                    <Image source={require('./assets/left.png')} style={styles.imageBack} />
                </TouchableOpacity>
                <View>
                    <Text style={styles.textLogin}>Please login using your account information to continue.</Text>
                </View>
                <View style={styles.wrapperInputButton}>
                    <TextInput style={styles.input1} placeholder={"Username"} />
                    <TextInput style={styles.input2} placeholder={"Password"} />
                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.textButton}>Masuk</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    wrapperInputButton: {
        marginVertical: 225,
        marginLeft: 41,
        marginRight: 72,
    },
    button: {
        backgroundColor: '#FFC700',
        height: 40,
        width: 200,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        left: 40,
    },
    textButton: {
        fontWeight: "bold",
        fontFamily: "Roboto",
        fontSize: 18
    },
    imagelogin: {
        height: 165,
        width: 125,
        position: "absolute",
        left: 225,
        top: 25
    },
    imageBack1: {
        height: 40,
        width: 40,
    },
    imageBack: {
        height: 25,
        width: 25,
        left: 20,
        top: 30
    },
    textLogin: {
        position: "absolute",
        width: 347,
        height: 42,
        left: 32,
        top: 150,
        fontFamily: "Roboto",
        fontStyle: "normal",
        fontWeight: "bold",
        fontSize: 18,
    },
    input1: {
        marginBottom: 72,
        borderWidth: 1,
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderLeftWidth: 0,
        borderColor: '#C4C4C4',
    },
    input2: {
        marginBottom: 70,
        borderWidth: 1,
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderLeftWidth: 0,
        borderColor: '#C4C4C4',
    }
})