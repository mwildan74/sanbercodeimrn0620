import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import YoutubeUi from './Tugas/Tugas12/App'
import Login from './Tugas/Tugas13/LoginScreen'
import About from './Tugas/Tugas13/AboutScreen'
import Todo from './Tugas/Tugas14/App'
import Skill from './Tugas/Tugas14/SkillScreen'
import Navigation from './Tugas/Tugas15/index'
import Navigation2 from './Tugas/TugasNavigation/index'
import Quiz3 from './Quiz3/index'


export default function App() {
  return (
    <Quiz3 />
    // <Navigation2 />
    // <Skill />
    // <Todo />
    // <YoutubeUi />
    // <Login />
    // <About />
    // <Navigation />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
