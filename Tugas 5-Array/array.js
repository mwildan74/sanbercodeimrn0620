// Nomber 1
console.log("=======Nomber1=======");
function range(startNum, finishNum) {
  if (!startNum || !finishNum) {
    var string = "-1";
    return string;
  } else if (startNum < finishNum) {
    var array = [];
    for (i = startNum; i <= finishNum; i++) {
      array.push(i);
    }
    return array;
  } else if (startNum > finishNum) {
    var array = [];
    for (i = startNum; i >= finishNum; i--) {
      array.push(i);
    }
    return array;
  }
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

// Nomber 2
console.log("=======Nomber2=======");

function rangeWithStep(startNum1, finishNum1, step) {
  var array = [];
  if (startNum1 < finishNum1) {
    for (var i = startNum1; i <= finishNum1; i += step) {
      array.push(i);
    }
  } else if (startNum1 > finishNum1) {
    for (var i = startNum1; i >= finishNum1; i -= step) {
      array.push(i);
    }
  }
  return array;
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

// Nomber 3
console.log("=======Nomber3=======");

function sum(startNum1, finishNum1, step = 1) {
  var fromArray = rangeWithStep(startNum1 || 0, finishNum1 || 0, step);
  var countArray = 0;
  for (var i = 0; i < fromArray.length; i++) {
    countArray += fromArray[i];
  }
  return countArray;
}
console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

//  Nomber 4

console.log("=======Nomber4=======");

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampug", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembirin", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

function dataHandling() {
  for (var i = 0; i < input.length; i++) {
    console.log("ID: " + input[i][(i, 0)]);
    console.log("Nama Lengkap :" + input[i][(i, 1)]);
    console.log("TTL :" + input[i][(i, 2)] + " " + input[i][(i, 3)]);
    console.log("Hobi : " + input[i][(i, 4)]);

    console.log(" ");
  }
}
dataHandling();

// Nomber 5
console.log("=======Nomber5=======");

function BalikKata(s) {
  var string = s;
  var newString = "";
  for (var i = string.length - 1; i >= 0; i--) {
    newString += string[i];
  }
  return newString;
}
console.log(BalikKata("Kasur Rusak"));
console.log(BalikKata("SanberCode"));
console.log(BalikKata("Haji Ijah"));
console.log(BalikKata("racecar"));
console.log(BalikKata("I am Sanbers"));

// Nomber 6
console.log("=======Nomber6=======")
function dataHandling2() {
  var data = [
    "0001",
    "Roman Alamsyah",
    "Bandar Lampung",
    "21/05/1989",
    "Membaca",
  ];
  data.splice(
    1,
    4,
    "Roman Alamsyah Elsharawy",
    "Provinsi Bandar Lampung",
    "21/05/1989",
    "Pria",
    "SMA Internasional Metro"
  );
  var split = data[3].split("/");
  var batas = data[1].slice(0, 15)
  var gabung = split.join("-");
  var urut = split.sort(function (a, b) {
    return parseInt(b) - parseInt(a);
  });
  var bulan = parseInt(urut[2]);
  switch (bulan) {
    case 1: { bulan = "Januari"; } break;
    case 2: { bulan = "Februari"; } break;
    case 3: { bulan = "Maret"; } break;
    case 4: { bulan = "April"; } break;
    case 5: { bulan = "Mei"; } break;
    case 6: { bulan = "Juni"; } break;
    case 7: { bulan = "Juli"; } break;
    case 8: { bulan = "Agustus"; } break;
    case 9: { bulan = "September"; } break;
    case 10: { bulan = "Oktober"; } break;
    case 11: { bulan = "November"; } break;
    case 12: { bulan = "Desember"; } break;

  }



  console.log(data);
  console.log(bulan);
  console.log(urut);
  console.log(gabung);
  console.log(batas);

}
dataHandling2();
