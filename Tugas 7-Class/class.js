// Nomber 1
console.log(`=======Nomber 1=======`)
class Animal {
    constructor(name) {
        this._name = name;
        this._legs = 4;
        this._ColdBlooded = false;
    }
    get name() {
        return this._name;
    }
    get legs() {
        return this._legs
    }
    get cold_blooded() {
        return this._ColdBlooded
    }

}

class Ape extends Animal {
    constructor(name) {
        super(name)
        this._name = name;
    }
    yell() {
        console.log("Auooo")
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
        this._name = name;
    }
    jump() {
        console.log("hop hop")
    }
}
var kodok = new Frog("buduk")
var sungokong = new Ape("Kera sakti")
var sheep = new Animal("shaun")

console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)
console.log(sungokong.name)
sungokong.yell()
console.log(kodok.name)
kodok.jump()

// Nomber 2
console.log(`=======Nomber2=======`)

class Clock {
    constructor({ template }) {
        this.template = template

    }
    render() {
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins
        var secs = date.getSeconds()
        if (secs < 10) secs = '0' + secs
        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs)

        console.log(output)
    }
    stop() {
        clearInterval(timer)
    }
    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000)
    }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start()