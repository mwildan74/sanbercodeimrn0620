// Nomber 1
console.log(`=======Nomber1=======`)
function arrayToObject(arr) {
    for (var i = 0; i < arr.length; i++) {

        var tahun = arr[i][3]
        var now = new Date()
        var thisYear = now.getFullYear()
        var result;
        if (tahun > thisYear || !tahun) {
            result = "Invalid Birth Year"
        } else {
            var umur = thisYear - tahun
            result = umur;
        }

        var bio = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: result

        }
        console.log(i + 1 + "." + bio.firstName + " " + bio.lastName)
        console.log(bio)
    }
}


var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)

// Nomber 2
console.log("=======Nomber2=======")
function shoppingTime(memberId, money) {

    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        var data = {
            memberId: memberId,
            money: money,
            listPurchesed: [],
            changeMoney: 0
        }

        var beli = true
        while (money > 0 && beli) {
            if (money >= 1500000) {
                data.listPurchesed.push("Sepatu Stacattu")
                money = money - 1500000
                beli = true
            } if (money >= 500000) {
                data.listPurchesed.push("Baju Zoro")
                money -= 500000
                beli = true
            } if (money >= 250000) {
                data.listPurchesed.push("Baju H&N")
                money -= 250000
                beli = true
            } if (money >= 175000) {
                data.listPurchesed.push("Sweater Uniklooh")
                money -= 175000
                beli = true
            } if (money >= 50000) {
                data.listPurchesed.push("Casing Handphone")
                money -= 50000
                beli = true
            }
            beli = false
        }
        data.changeMoney = money
        return data;
    }
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000))
console.log(shoppingTime());

// nomber 3
console.log("======Nomber3=======")
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    var simpan = []
    for (var i = 0; i < arrPenumpang.length; i++) {

        var penumpang = arrPenumpang[i][0];
        var naikDari = rute.indexOf(arrPenumpang[i][1]);
        var tujuan = rute.indexOf(arrPenumpang[i][2]);
        var bayar = (tujuan - naikDari) * 2000;

        var listPenumpang = {
            penumpang: penumpang,
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: bayar
        }
        simpan.push(listPenumpang)
    }
    return simpan
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([]));